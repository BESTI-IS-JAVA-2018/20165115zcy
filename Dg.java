public class Dg {
	public static void main(String [] args) {
		int i=0,sum=0;
		for(String arg : args)
			i = Integer.parseInt(arg);
		if(fact(i)!=-1)
			sum=sum+fact(i);
		else if(fact(i)== -1||fact(i)== 0)
			System.out.println("error!");
		System.out.println(sum);
	}
	public static int fact(int n) {
		if(n==0)
			return 1;
		else if(n<0)
			return -1;
		else
			return n*fact(n-1);
	}
}
