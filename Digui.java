import java.util.*; 
public class Digui {
    public static void main(String[] args){
       try{ 
           Scanner input = new Scanner(System.in); 
           String num = input.nextLine();
           if(!num.match("[^0]\\d+$")) {
               throw new RuntimeException("输入的不是自然数");
           } 
           if(Long.parseLong(num) >20) {
               throw new RuntimeException("数字过大，无法计算");   
            }
            long result = factorial(Long.parseLong(num)); 
             System.out.println(result); 
        }catch(Exception e){
            throw new RuntimeException(e);
        }finally{
            System.out.println("Finish computing factorial number"); 
        }
    } 
    public static long  factorial(long n) {
        if(n==1) return 1 ;  
        return n * factorial(n-1); 
    }  
}
