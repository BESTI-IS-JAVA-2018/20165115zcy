public class exp1 {
        // 最大公约数
        public static int get_gcd(int a, int b) {
            int max, min;
            max = (a > b) ? a : b;
            min = (a < b) ? a : b;

            if (max % min != 0) {
                return get_gcd(min, max % min);
            } else
                return min;

        }

        // 最小公倍数
        public static int get_lcm(int a, int b) {
            return a * b / get_gcd(a, b);
        }

        public static void main(String[] args) {
            int [] tmp = new int [args.length];
            for(int i=0; i<args.length;i++) {
                tmp[i]= Integer.parseInt(args[i]);
            }
            int n1 = tmp[0];
            int n2 = tmp[1];
            System.out.println("(" + n1 + "," + n2 + ")" + "=" + get_gcd(n1, n2));
            System.out.println("[" + n1 + "," + n2 + "]" + "=" + get_lcm(n1, n2));

        }

    }

