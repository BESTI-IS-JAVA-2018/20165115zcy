public class MyComplex {
    double a, b;

    MyComplex(double m, double n) {//构造函数设置实部虚部
        a = m;
        b = n;
    }
    public double getRealPart() {//返回实部
        return a;
    }
    public double getImagePart() {//返回虚部
        return b;
    }
    public MyComplex ComplexAdd(MyComplex y) {//加法
        double m = y.getRealPart();
        double n = y.getImagePart();
        double x = a + m;
        double z = b + n;
        return new MyComplex(x, z);
    }
    public MyComplex ComplexSub(MyComplex y) {
         double m = y.getRealPart();
         double n = y.getImagePart();
         double x = a - m;
         double z = b - n;
         return new MyComplex(x, z);
    }
    public MyComplex ComplexMulti(MyComplex y) {
         double m = y.getRealPart();
         double n = y.getImagePart();
         double x = a * m;
         double z = b * n;
         return new MyComplex(x, z);
    }
    public MyComplex ComplexDiv(MyComplex y) {
         double m = y.getRealPart();
         double n = y.getImagePart();
         double x = a / m;
         double z = b / n;
         return new MyComplex(x, z);
    }
    @Override
    public java.lang.String toString() {
        String s = "";
        if (a != 0 && b > 0 && b != 1) {
            s += a + "+" + b + "i";
        }
        else if (a != 0 && b == 1) {
            s += a + "+i";
        }
        else if (a != 0 && b < 0 && b != -1) {
            s += a + "" + b + "i";
        }
        else if (a != 0 && b == -1) {
            s += a + "-i";
        }
        else if (a != 0 && b == 0) {
            s += a;
        }
        else if (a == 0 && b != 0) {
            s += b + "i";
        }
        else if (a == 0 && b == 0) {
            s += "0.0";
        }
        return s;
    }
}

