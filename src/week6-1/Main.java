public class Main {
	public static void main(String[] args){
		int m;
		int n;
		try {
			n=Integer.parseInt(args[0]);
			m=Integer.parseInt(args[1]);
			if(m>n)
				throw new java.io.IOException("n>m");
				System.out.println("C(n,m)="+C(n,m));
		}
		catch(NumberFormatException e) {
			System.out.println("异常输入:"+e.getMessage());
 		}
		catch(ArrayIndexOutOfBoundsException e){
			System.out.println("异常输入：请输入两个数字");
		}
		catch(java.io.IOException e){
			System.out.println("异常输入："+e.getMessage());
		}
	}
static int C(int n,int m){
	if(n==0||m==0||n==m)
		return 1;
		return C(n-1,m-1)+C(n-1,m);
}
}	
