public class Test {
	public static void main(String[] args) {
		CPU cpu=new CPU();
		CPU cpu2=new CPU();
		cpu.speed=2200;
		cpu2.speed=2300;
		HardDisk disk=new HardDisk();
		HardDisk disk2=new HardDisk();
		disk.amount=200;
		disk2.amount=200;
		PC pc=new PC();
		PC pc2=new PC();
		pc.setCPU(cpu);
		pc.setHardDisk(disk);
		pc.show();
		pc2.setCPU(cpu2);
		pc2.setHardDisk(disk2);
		System.out.println("pc2。toString()="+pc2.toString());
		System.out.println("cpu2。toString()="+cpu2.toString());
		System.out.println("disk2。toString()="+disk2.toString());
		System.out.println("PC.equals(pc,pc2)="+PC.equals(pc,pc2));
		System.out.println("CPU.equals(cpu,cpu2)="+CPU.equals(cpu,cpu2));
		System.out.println("HardDisk.equals(disk,disk2)="+HardDisk.equals(disk,disk2));
	}
}
