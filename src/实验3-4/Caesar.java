public class Caesar {
    private static Caesar caesar = new Caesar();

    public static Caesar getInstance() {
        return caesar;
    }

    private Caesar() {
    }

    public String encrypt(String str, int k) {
        StringBuilder result = new StringBuilder();
        for (char c : str.toCharArray()) {
            // 字符中出现小写字母
            if (c >= 'a' && c <= 'z') {
                c += k % 26;
                if (c < 'a') c += 26;
                if (c > 'z') c -= 26;
            }
            // 字符中出现大写字母
            else if (c >= 'A' && c <= 'Z') {
                c += k % 26;
                if(c < 'A')  c += 26;
                if(c > 'Z')  c -= 26;
            }
            result.append(c);
        }
        return result.toString();
    }
    public String decrypt(String str, int k){
        k = 0 - k;
        return encrypt(str,k);
    }
}
