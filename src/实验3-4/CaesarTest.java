import junit.framework.TestCase;
import org.junit.Test;
import junit.framework.TestResult;

public class CaesarTest{
    Caesar caeser = Caesar.getInstance();

   @Test
    public void encode(){
       String data = "zhangchengyu";
       int k = 4;
       String result = caeser.encrypt(data,k);
       System.out.println("加密后："+ result);
       System.out.println("解密后："+ caeser.decrypt(result,k));
    }
}